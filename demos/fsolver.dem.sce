mode(-1); 
demodir = get_absolute_file_path('fsolver.dem.sce');
funcprot(0);
printf('Starting Demo\n');

function y=scilab_f(x) 
  y = (x-1).^2
endfunction 

x0 = [3; 2];

printf('\nThis is the most basic call to fsolver\n');

[x,fval,exitflag,status] = fsolver(scilab_f,x0);

printf('x        = \n'); disp(x); 
printf('fval     = \n'); disp(fval);
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));


printf('\n\nNow we call fsolver with some options and a dense Jacobian\n'); 

options = init_param();
options = add_param(options,'TolFun',1e-9);
options = add_param(options,'Jacobian','on');
options = add_param(options,'DerivativeCheck','on');

function [f,J] = scilab_fj(x)
    f = (x-1).^2;
    [nargout, nargin] = argn();
    //printf('scilab_fj called with out: %d in:%d\n',nargout, nargin);
    if (nargout > 1) 
        J = diag(2.*(x-1));
    end  
endfunction 

[x,fval,exitflag,status] = fsolver(scilab_fj,x0,options);

printf('x        = \n'); disp(x); 
printf('fval     = \n'); disp(fval);
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));


printf('\n\nNow we call fsolve with a sparse jacobian\n');

options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','on');
options = add_param(options,'DerivativeCheck','on'); 

function [f,J] = scilab_sparse_fj(x)
    f = (x-1).^2;
    [nargout, nargin] = argn();
    //printf('scilab_sparse_fj called with out: %d in:%d\n',nargout, nargin);
    if nargout > 1
        J = diag(sparse(2.*(x-1)));
    end 
endfunction 

[x,fval,exitflag,status] = fsolver(scilab_sparse_fj,x0,options);

printf('x        = \n'); disp(x); 
printf('fval     = \n'); disp(fval);
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));


printf('\n\nNow we call fsolve with another sparse jacobian\n');

options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','on');
options = add_param(options,'Display','iter');
//options = add_param(options,'DerivativeCheck','on'); 
x0 = [-3; 4];
function [f,J] = powell(x) 
   f(1) = 10*(x(2) - x(1)^2); f(2) = 1 - x(1);
   J = sparse([ -20*x(1) 10; -1 0]);
endfunction 
[x,fval,exitflag,status] = fsolver(powell,x0,options);

printf('x        = \n'); disp(x); 
printf('fval     = \n'); disp(fval);
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));


// Collect data on each of the solvers 
nproblems = 6;
nsolvers  = 4;

runtimes = zeros(nproblems,nsolvers);
numfeval = zeros(nproblems,nsolvers);
residual = zeros(nproblems,nsolvers);

// test_problem calls each of the solvers on the same test problem
function [residual,numfeval,runtimes] = test_problem(name,file,func,funcdense,n,x0)
printf('\n\nRunning %s demo\n',name);
options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','off');
solver = 1;
printf('\n\nSolving system of %d equations with no derivatives\n',n);

tic();
[x,fval,exitflag,status] = fsolver(func,x0,options);
timetaken = toc(); 

printf('norm(x)  = %e\n', norm(x));
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));
printf('time     = %g\n', timetaken);
residual(solver) = norm(fval);
numfeval(solver) = status('nfev');
runtimes(solver) = timetaken;


options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','on');
//options = add_param(options,'DerivativeCheck','on');
solver = 2;
printf('\n\nSolving system of %d equations with dense derivatives\n',n);

tic();
[x,fval,exitflag,status] = fsolver(funcdense,x0,options);
timetaken = toc(); 

printf('norm(x)  = %e\n', norm(x));
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));
printf('time     = %g\n', timetaken);
residual(solver) = norm(fval);
numfeval(solver) = status('nfev');
runtimes(solver) = timetaken;


options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','on');
//options = add_param(options,'DerivativeCheck','on');
options = add_param(options,'Display','iter');
solver = 3;
printf('\n\nSolving system of %d equations with sparse derivatives\n',n);

tic();
[x,fval,exitflag,status] = fsolver(func,x0,options);
timetaken = toc(); 

printf('norm(x)  = %e\n', norm(x));
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));
printf('time     = %g\n', timetaken);
residual(solver) = norm(fval);
numfeval(solver) = status('nfev');
runtimes(solver) = timetaken;


[f,J] = func(x0);
options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','off');
options = add_param(options,'JacobPattern',spones(J));
solver = 4;
printf('\n\nSolving system of %d equations with sparse finite-differences\n',n);

tic();
[x,fval,exitflag,status] = fsolver(funcdense,x0,options);
timetaken = toc();

printf('norm(x)  = %e\n', norm(x));
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));
printf('time     = %g\n', timetaken);
residual(solver) = norm(fval);
numfeval(solver) = status('nfev');
runtimes(solver) = timetaken;

endfunction

// before we start the demo increase the stacksize 
sz = stacksize(); 
newsize = 134217728;
stacksize(newsize);


//////////////////////////////////////
// Generalized Rosenbrock function 
/////////////////////////////////////
problem = 1;
name = 'Generalized Rosenbrock';
file  ='generalrosenbrock.sci';
exec(demodir+file);
// uncomment for good test 
//n = 2^12;
// this is a smaller test 
n = 2^10
x0(1:n,1) = -1.9;
x0(2:2:n,1) = 2; 
func = genrosenbrock;
funcdense = genrosenbrockdense;
[residualp,numfevalp,runtimesp] = test_problem(name,file,func,funcdense,n,x0);
residual(problem,:) = residualp';
numfeval(problem,:) = numfevalp';
runtimes(problem,:) = runtimesp';

/////////////////////////////////////////////////
/// Bryoden Tridiagonal function 
////////////////////////////////////////////////
problem = 2;
name = 'Broydens Tridiagonal';
file = 'broydentridiagonal.sci';
exec(demodir+file);
n = 1000; 
x0 = -ones(n,1);
func = broydentridiagonal;
funcdense = broydentridiagonaldense;
[residualp,numfevalp,runtimesp] = test_problem(name,file,func,funcdense,n,x0);
residual(problem,:) = residualp';
numfeval(problem,:) = numfevalp';
runtimes(problem,:) = runtimesp';



////////////////////////////////////////////////
// TrigExp function 
///////////////////////////////////////////////
problem = 3;
name = 'TrigExp';
file = 'trigexp.sci';
exec(demodir+file);
n = 1000; 
x0 = 0.3*ones(n,1);
func = trigexp;
funcdense = trigexpdense;
[residualp,numfevalp,runtimesp] = test_problem(name,file,func,funcdense,n,x0);
residual(problem,:) = residualp';
numfeval(problem,:) = numfevalp';
runtimes(problem,:) = runtimesp';



////////////////////////////////////////////////
// Function6 function 
///////////////////////////////////////////////
problem = 4;
name = 'Function6 function';
file = 'function6.sci';
exec(demodir+file);
n = 1000; 
x0 = -ones(n,1);
func = function6;
funcdense = function6dense;
[residualp,numfevalp,runtimesp] = test_problem(name,file,func,funcdense,n,x0);
residual(problem,:) = residualp';
numfeval(problem,:) = numfevalp';
runtimes(problem,:) = runtimesp';


////////////////////////////////////////////////
// Singular Broyden function 
///////////////////////////////////////////////
problem = 5;
name = 'Singular Broyden';
file = 'singularbroyden.sci';
exec(demodir+file);
n = 1000; 
x0 = -ones(n,1);
func = singularbroyden;
funcdense = singularbroydendense;
[residualp,numfevalp,runtimesp] = test_problem(name,file,func,funcdense,n,x0);
residual(problem,:) = residualp';
numfeval(problem,:) = numfevalp';
runtimes(problem,:) = runtimesp';

////////////////////////////////////////////////
// Powell's singular problem
////////////////////////////////////////////////
problem = 6;
name = 'Powells singular ';
file = 'powellsingular.sci';
exec(demodir+file);
n = 1000; 
x0 = [3;1];
func = powellsingular;
funcdense = powellsingulardense;
[residualp,numfevalp,runtimesp] = test_problem(name,file,func,funcdense,n,x0);
residual(problem,:) = residualp';
numfeval(problem,:) = numfevalp';
runtimes(problem,:) = runtimesp';


////////////////////////////////////////////////
// Bad Broyden function 
///////////////////////////////////////////////
printf('\n\nBad Broyden function\n');
exec(demodir+'badbroyden.sci');

n = 100; 
x0 = -ones(n,1);

options = init_param();
options = add_param(options,'TolFun',1e-2);
options = add_param(options,'Jacobian','on');
options = add_param(options,'DerivativeCheck','on');

printf('\n\nSolving system of %d equations with dense derivatives\n',n);
printf('Note there is an error in the Jacobian matrix\n');
printf('Here we turn on the derivative check to see if this error is caught\n');
tic();
[x,fval,exitflag,status] = fsolver(badbroydendense,x0,options);
timetaken = toc(); 

printf('norm(x)  = %e\n', norm(x));
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));
printf('time     = %g\n', timetaken);


printf('End of Demo\n');

unix('rm residual.out'); write('residual.out',residual);
unix('rm numfeval.out'); write('numfeval.out',numfeval);
unix('rm runtimes.out'); write('runtimes.out',runtimes);
stacksize(sz(1));
