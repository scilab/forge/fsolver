mode(-1);
demodir = get_absolute_file_path('fsolver_sfd.dem.sce');
exec(demodir+'generalrosenbrock.sci');

// this is a small test 
n = 4;
x0(1:n,1) = -1.9;
x0(2:2:n,1) = 2; 

[f,J] = genrosenbrock(x0);
J = spones(J);
disp(J);
options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','off');
options = add_param(options,'JacobPattern',J);

printf('\n\nSolving system of %d equations with no derivatives\n',n);

tic();
[x,fval,exitflag,status] = fsolver(genrosenbrock,x0,options);
timetaken = toc(); 

printf('norm(x-1)  = %e\n', norm(x-1));
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));
printf('time     = %g\n', timetaken);
