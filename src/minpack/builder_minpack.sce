// ====================================================================
// Yann COLLETTE
// Christopher MAES
// Copyrigth 2009
// This file is released into the public domain
// ====================================================================

exported_symbols = ['hybrj1','hybrd1'];
files_to_compile = ['hybrd.f','hybrd1.f', 'hybrj.f','hybrj1.f', ...
		    'dogleg.f','enorm.f','fdjac1.f','qrfac.f','qform.f','chkder.f'];

libs = '';
ldflags = '';
cflags = '';
fflags = '';
cc = '';
libname = 'minpack';
tbx_build_src(exported_symbols, files_to_compile, 'f', ...
              get_absolute_file_path('builder_minpack.sce'), ...
	      libs,ldflags,cflags,fflags,cc,libname);

clear tbx_build_src;

